/** @jsx jsx */
import FsLayout from '@/fs/FsLayout';
import { jsx } from '@emotion/react';
import { PageProps } from 'gatsby';
import React from 'react';

const Letters: React.FC<PageProps> = ({ location }) => (
  <FsLayout location={location}>
    <p>letters</p>
  </FsLayout>
);

export default Letters;
