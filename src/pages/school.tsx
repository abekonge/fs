/** @jsx jsx */
import FsLayout from '@/fs/FsLayout';
import { jsx } from '@emotion/react';
import { PageProps } from 'gatsby';
import React from 'react';

const School: React.FC<PageProps> = ({ location }) => (
  <FsLayout location={location}>
    <p>skole</p>
  </FsLayout>
);

export default School;
