/** @jsx jsx */
import FsLayout from '@/fs/FsLayout';
import { css, jsx } from '@emotion/react';
import { PageProps } from 'gatsby';
import React from 'react';

const Connect: React.FC<PageProps> = ({ location }) => (
  <FsLayout location={location}>
    <form
      method="POST"
      action="https://gaia.chat/mailman/cgi-bin/subscribe/gossip"
      id="tilmeld"
    >
      <div
        css={css`
          display: flex;
          flex-direction: column;
          gap: 0.5rem;
          max-width: 25rem;
        `}
      >
        {/* <h1>Få besked:</h1> */}
        <label
          htmlFor="email"
          css={css`
            display: flex;
            /* justify-content: space-between; */
          `}
        >
          <section
            css={css`
              width: 10rem;
            `}
          >
            email:
          </section>
          <input type="Text" name="email" id="email" />
        </label>
        <label
          htmlFor="fullname"
          css={css`
            display: flex;
            /* justify-content: space-between; */
          `}
        >
          <section
            css={css`
              width: 10rem;
            `}
          >
            navn (valgfrit):
          </section>
          <input type="Text" name="fullname" id="fullname" />
        </label>
        <section
          css={css`
            text-align: right;
          `}
        >
          <input
            type="reset"
            name="email-button"
            value="Lyt"
            onClick={() => document.forms[0].submit()}
          />
        </section>
        <div
          css={css`
            text-align: right;
            margin-top: 1rem;
          `}
        >
          {/* <h1> */}
          <a
            href="mailto:collective@futureschool.earth"
            // css={css`
            //   font-size: 2rem;
            //   &,
            //   &:active,
            //   &:visited {
            //     /* color: blue; */
            //   }
            // `}
          >
            collective@futureschool.earth
          </a>
          {/* </h1> */}
        </div>
      </div>
    </form>
  </FsLayout>
);

export default Connect;
