/** @jsx jsx */
import FsLayout from '@/fs/FsLayout';
import { Manifest } from '@/fs/Manifest';
import { jsx } from '@emotion/react';
import { PageProps } from 'gatsby';
import React from 'react';

const FS: React.FC<PageProps> = ({ location }) => (
  <FsLayout location={location}>
    <Manifest />
  </FsLayout>
);

export default FS;
