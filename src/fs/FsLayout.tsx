/** @jsx jsx */
import { css, jsx } from '@emotion/react';
import { Link } from 'gatsby';
import React, { useEffect, useState } from 'react';
import Layout from '../components/layout';
import Page from '../components/page';
import Screen from '../components/screen';
import { Logo } from './Logo';
import { Nav } from './Nav';
import { undesign } from './Paletter';

const FSLayout: React.FC<{ location: any; className?: string }> = ({
  location,
  children,
  className,
}) => {
  const [palette, sætPalette] = useState(undefined);

  useEffect(() => sætPalette(undesign()), []);
  return (
    <Layout>
      {/* <PaletteVælger palette={palette} sætPalette={sætPalette} /> */}
      {palette && (
        <Screen
          css={css`
            background-color: ${palette.baggrund};
            color: ${palette.afsnit};
            & a.linksy {
              color: ${palette.link};
            }
          `}
        >
          <Page className={className}>
            <Link
              to="/"
              onClick={() => sætPalette(undesign())}
              css={css`
                text-decoration: none;
              `}
            >
              <Logo palette={palette} />
            </Link>
            <Nav
              location={location}
              palette={palette}
              sætPalette={sætPalette}
            />

            {children}
          </Page>
        </Screen>
      )}
    </Layout>
  );
};

export default FSLayout;
