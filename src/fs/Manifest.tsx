/** @jsx jsx */
import { css, jsx } from '@emotion/react';

export const Manifest: React.FC = () => (
  <section>
    <p>
      Future School er et Svendborg/Møn-baseret læringsprojekt, der spørger til,
      hvad vi har brug for at kunne i fremtiden. Hvad må vi lære for at kunne
      bebo fremtidens sårbare, ødelagte landskaber med vores sårbare, ødelagte
      kroppe på nye, gamle, nænsomme, genopbyggende måder? Hvilke former for
      viden må vi tilegne os for at kultivere andre mulige fremtider end den,
      som den petrokapitalistiske orden har stillet os i udsigt? Hvordan kan vi
      reparere det, som er blevet beskadiget, aflære hvidhedens vold, afmontere
      det nekropolitiske Vest, vores ødelæggende arv, drage hinanden til ansvar,
      tilgive, vedkende os fejl, tvivle, spørge, nære? Hvordan kan vi installere
      en reparativ retfærdighed, og betale vores gæld tilbage?
    </p>
    <p>
      Vi vil holde solidariske, omsorgsfulde rum for-, og tage vare på hinanden,
      så vi kan blive farlige sammen. Vi vil mere end bare at overleve i denne
      verdens ruiner, vi vil blomstringen, fortryllelsen, det meningsfulde
      arbejde og den nødvendige hvile. Vi vil tilegne os nye såvel som gamle
      former for viden og praksis, der eksisterer i strid med eller på trods af
      kapitalismens logikker, skjult, glemt, gemt. Vi vil finde sammen, finde
      sted og håb, lære af og med hinanden, gøre- og blive-andet. Vi vil skabe
      nye forbindelser og knytte bånd på tværs af kroppe, geografier,
      generationer og steder, i øjeblikkelige lommer af læring og viden og
      venskab.
    </p>
    <p>
      Future School er en mobil og polymorf skole for fælles sårbarhed, for
      radikale omsorgsformater, for revolutionært venskab, oprørsk glæde og blød
      modstand. Det er et sted, hvor vi kan blive med tvivlen, blive i besværet
      med at af- og genlære i åbne, gæstfri og porøse fællesskaber, der kan
      danne afsæt for en flerhed af vildtvoksende virkeliggjorte utopiske
      nutider.
    </p>
    <p
      css={css`
        font-size: 0.9rem;
      `}
    >
      Fremtidens Skole er et radikalt pædagogisk samarbejde mellem{' '}
      <a
        className="linksy"
        target="_blank"
        rel="noreferrer"
        href="http://futurologi.org"
      >
        Center for Militant Futurologi
      </a>{' '}
      og{' '}
      <a
        className="linksy"
        href="https://labae.org"
        target="_blank"
        rel="noreferrer"
      >
        {' '}
        Laboratoriet for Æstetik og Økologi
      </a>
      .
    </p>
  </section>
);
