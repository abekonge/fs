/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/** @jsx jsx */
import { css, jsx } from '@emotion/react';
import { Palette } from './Paletter';

export const Logo: React.FC<{ palette: Palette }> = ({ palette }) => (
  <main
    css={css`
      margin-top: -1rem;
      color: ${palette.logo};
      font-size: 4rem;
      font-family: 'Lyno Stan';
      line-height: 1;
      /* background-color: #49a7cf;
      color: #ba2c2f; */
    `}
  >
    <section>FUTURE SCHOOL</section>
  </main>
);
