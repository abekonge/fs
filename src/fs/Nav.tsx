/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/anchor-is-valid */
/** @jsx jsx */
import { css, jsx } from '@emotion/react';
import { Link } from 'gatsby';
import { Palette } from './Paletter';

export const Nav: React.FC<{
  location: any;
  palette: Palette;
  sætPalette: (palettte: Palette) => void;
}> = ({ location, palette, sætPalette }) => (
  <nav
    css={css`
      margin-top: 0.5rem;
      margin-bottom: 2rem;
      color: ${palette.accent};
    `}
  >
    <Link
      to="/"
      className="blip"
      style={{ color: palette.accent }}
      activeStyle={{ backgroundColor: palette.fremhæv }}
      // css={css`
      //   background-color: ${location.pathname === '/'
      //     ? palette.fremhæv
      //     : 'inherit '};
      // `}
    >
      manifest
    </Link>{' '}
    —{' '}
    {/* <Link
      to="/school"
      style={{ color: palette.accent }}
      activeStyle={{ backgroundColor: palette.fremhæv }}
      // css={css`
      //   background-color: ${location.pathname === '/school'
      //     ? palette.fremhæv
      //     : 'inherit '};
      // `}
    >
      skole
    </Link>{' '} */}
    {/* —{' '}
    <Link
      to="/letters"
      style={{ color: palette.accent }}
      activeStyle={{ backgroundColor: palette.fremhæv }}
      // css={css`
      //   background-color: ${location.pathname === '/letters'
      //     ? palette.fremhæv
      //     : 'inherit '};
      // `}
    >
      breve
    </Link>{' '} */}
    {/* —{' '} */}
    <Link
      to="/connect"
      style={{ color: palette.accent }}
      activeStyle={{ backgroundColor: palette.fremhæv }}

      // css={css`
      //   background-color: ${location.pathname === '/connect'
      //     ? palette.fremhæv
      //     : 'inherit '};
      // `}
    >
      forbind
    </Link>
    {/*     
    {' '}
    —{' '}
    <a
      css={css`
        color: ${palette.accent};
        text-decoration: underline;
        cursor: pointer;
      `}
      onClick={() => sætPalette(undesign())}
    >
      forandr
    </a> */}
  </nav>
);
