/* eslint-disable no-alert */
/** @jsx jsx */
import { erLæsBar } from './colorHelpers';

export const farver: { [colorName: string]: string } = {
  mørkegrå: '#A9A9A9',
  hvid: '#ffffff',
  blå: '#1E00FF',
  tavle: '#282828',
  pinky: '#ff0080',
  snowFlurry: '#d1ffc1',
  deepCove: '#150649',
  diSerria: '#e19153',
  apple: '#3dc333',
  hotToddy: '#a27a07',
  zombie: '#e2de8e',
  espresso: '#5c2715',
  indigo: '#4f78c7',
  sårt: '#000000',
  thunderbird: '#d1351f',
  jordyBlue: '#7cb7f1',
  steelBlue: '#4674bc',
  emerald: '#5fc67a',
  salem: '#049342',
  funGreen: '#006d2f',
  tuscany: '#bd602e',
  englishHolly: '#042a19',
  espresso2: '#5f2b18',
  silverRust: '#c9c2bc',
  acapulco: '#6faca6',
  ebonyClay: '#24323b',
};

export interface Palette {
  fremhæv: string;
  baggrund: string;
  afsnit: string;
  accent: string;
  logo: string;
  link: string;
}

// export const paletter: {
//   [name: string]: Palette;
// } = {
//   tavle: {
//     logo: farver.mørkegrå,
//     afsnit: farver.hvid,
//     fremhæv: farver.blå,
//     baggrund: farver.tavle,
//     accent: farver.pinky,
//     link: farver.pinky,
//   },
//   opal: {
//     baggrund: farver.snowFlurry,
//     logo: farver.indigo,
//     afsnit: farver.espresso,
//     fremhæv: farver.apple,
//     accent: farver.sårt,
//     link: farver.sårt,
//   },
//   espresso: {
//     baggrund: farver.espresso,
//     logo: farver.hotToddy,
//     afsnit: farver.snowFlurry,
//     fremhæv: farver.indigo,
//     accent: farver.apple,
//     link: farver.apple,
//   },
//   espresso2: {
//     baggrund: farver.hotToddy,
//     logo: farver.espresso,
//     afsnit: farver.snowFlurry,
//     fremhæv: farver.snowFlurry,
//     accent: farver.snowFlurry,
//     link: farver.apple,
//   },
//   stones: {
//     baggrund: farver.thunderbird,
//     logo: farver.jordyBlue,
//     afsnit: farver.hvid,
//     accent: farver.sårt,
//     link: farver.emerald,
//     fremhæv: farver.emerald,
//   },
// };

// const timeRandom = seedrandom(String(new Date()));

const tagTilfældigFarve = (kontrastFarve?: string) => {
  const farveListe = Object.keys(farver).map((navn) => farver[navn]);
  const farveAntal = farveListe.length;
  // console.log('timerandom', timeRandom());
  const tilfældigFarve =
    // farveListe[Math.floor(timeRandom() * 100) % farveAntal];
    farveListe[Math.floor(Math.random() * 100) % farveAntal];
  if (kontrastFarve) {
    return erLæsBar(kontrastFarve, tilfældigFarve)
      ? tilfældigFarve
      : tagTilfældigFarve(kontrastFarve);
  }
  return tilfældigFarve;
};

export const undesign = (): Palette => {
  const baggrund = tagTilfældigFarve();
  const logo = tagTilfældigFarve(baggrund);
  const afsnit = tagTilfældigFarve(baggrund);
  const accent = tagTilfældigFarve(baggrund);
  const fremhæv = tagTilfældigFarve(accent);
  const link = tagTilfældigFarve(baggrund);

  const undesigned = {
    accent,
    afsnit,
    baggrund,
    fremhæv,
    logo,
    link,
  };

  return undesigned;
};

// export const PaletteVælger: React.FC<{
//   palette: Palette;
//   sætPalette: (palette: Palette) => void;
// }> = ({ palette, sætPalette }) => (
//   <section
//     css={css`
//       display: flex;
//       flex-direction: column;
//       position: absolute;
//       right: 0;
//       top: 0;
//     `}
//   >
//     <select
//       name="paletter"
//       id="palette"
//       onChange={(event) => sætPalette(paletter[event.target.value])}
//     >
//       <option value="tavle">Tavle</option>
//       <option value="espresso">Espresso</option>
//       <option value="espresso2">Espresso2</option>
//       <option value="stones">Stones</option>
//     </select>
//     {Object.keys(paletter.tavle).map((aspekt) => (
//       <select
//         key={aspekt}
//         id={aspekt}
//         name={aspekt}
//         onChange={(event) =>
//           sætPalette({ ...palette, [aspekt]: event.target.value })
//         }
//       >
//         {Object.keys(farver).map((farve) => (
//           <option
//             value={farver[farve]}
//             selected={palette[aspekt] === farver[farve]}
//           >
//             {farve}
//           </option>
//         ))}
//       </select>
//     ))}
//     <button type="button" onClick={() => sætPalette(undesign())}>
//       UNDESIGN
//     </button>
//   </section>
// );
