import { css } from '@emotion/core';
import React from 'react';

const Slip = ({ className }) => (
  <p
    className={className}
    css={css`
      text-align: center;
      font-size: 2rem;
      font-weight: 700;
      margin-bottom: -0.15rem;
    `}
  >
    ꔫ
  </p>
);
export default Slip;
