import { css } from '@emotion/react';
// import PropTypes, { element } from 'prop-types';
import React, { FC, ReactElement } from 'react';

const Band: FC<{
  className: string;
  left: ReactElement;
  right: ReactElement;
}> = ({ children, className, left, right }) => (
  <section
    className={className}
    css={css`
      display: flex;
      align-items: center;
      justify-content: space-between;
      height: 2.5rem;
      background-color: black;
      color: white;
    `}
  >
    <aside
      css={css`
        box-sizing: border-box;
        width: 2rem;
        padding-left: 0.5rem;
        padding-right: 1rem;
      `}
    >
      {left}
    </aside>
    {children}
    <aside
      css={css`
        width: 2rem;
      `}
    >
      {right}
    </aside>
  </section>
);

export default Band;
