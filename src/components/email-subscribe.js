import { css } from '@emotion/core';
import React, { useState } from 'react';

const EmailSubscribeForm = ({ autoFocus, className, onSucces }) => {
  const [value, setValue] = useState('');
  const [sending, setSending] = useState(false);
  const [succes, setSucces] = useState(false);
  const [error, setError] = useState('');

  const emailRegExp = /^\w+([.-]?\w+)@\w+([.-]?\w+)(.\w{2,3})+$/;

  const subscribe = () => {
    setSending(true);
    window
      .fetch(
        'https://campchristiania.hosted.phplist.com/lists/?p=subscribe&id=1',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: `email=${window.encodeURI(
            value,
          )}&htmlemail=1&list%5B3%5D=signup&listname%5B3%5D=camp+christiania+&VerificationCodeX=&subscribe=Subscribe+to+the+selected+newsletters`,
        },
      )
      .then((res) => {
        if (res.status === 200) {
          setSucces(emailRegExp.test(value));
          onSucces();
        } else {
          console.log('no dice');
          setError(res.statusText);
        }
        setSending(false);
      })
      .catch((err) => {
        setError('Fejl: ' + String(err));
        setSending(false);
      });
  };

  const emailForm = (
    <div
      css={css`
        display: flex;
        justify-content: center;
      `}
    >
      <input
        // eslint-disable-next-line jsx-a11y/no-autofocus
        autoFocus={autoFocus}
        value={value}
        onChange={(event) => setValue(event.target.value)}
        disabled={sending}
        css={css`
          padding: 0.3rem;
          font-family: CMU Sans Serif;
          border: none;
          border-radius: 0.2rem;
          font-size: 0.9rem;
          max-width: 15rem;
        `}
        placeholder="skriv din emailadresse"
      />
      <button
        onClick={() => {
          subscribe();
        }}
        type="button"
        disabled={!emailRegExp.test(value)}
        css={css`
          margin-left: 0.5rem;
          background-color: teal;
          max-width: 5rem;
          border: none;
          color: white;
          border-radius: 0.2rem;
          ${emailRegExp.test(value)
            ? css`
                cursor: pointer;
                :hover {
                  background-color: purple;
                }
                :active {
                  border: 2px solid whitesmoke;
                }
              `
            : css`
                background-color: lightgray;
                color: gray;
              `}
        `}
      >
        Skyd
      </button>
    </div>
  );

  const sendingDisplay = (
    <p>
      <em>Sender...</em>
    </p>
  );

  const succesDisplay = (
    <div>
      <p>Det lykkedes!</p>
      <p>
        <strong>Hvis du ikke får en email nu, så tjek spam!</strong>
      </p>
    </div>
  );

  const errorDisplay = <p>{error}</p>;

  return (
    <div
      css={css`
        padding-bottom: 2rem;
      `}
    >
      <section
        className={className}
        css={css`
          padding: 2rem;
          border-radius: 0.4rem;
          border: 1px solid whitesmoke;
          background-color: tomato;
          text-align: center;
          color: white;
        `}
      >
        <h2>Følg med i Camp Christiania</h2>
        <p
          css={css`
            color: black;
          `}
        >
          Modtag nye episoder på email
        </p>

        {(sending && sendingDisplay) ||
          (succes && succesDisplay) ||
          (error && errorDisplay) ||
          emailForm}

        <p
          css={css`
            font-size: 0.8rem;
            color: white;
            margin: 1rem 0 0 0;
          `}
        >
          Der kommer en ny episode ca. en gang om ugen.
        </p>
      </section>
    </div>
  );
};

EmailSubscribeForm.defaultProps = {
  autoFocus: false,
  className: '',
  onSucces: () => console.log('Anmodning sendt!'),
};

export default EmailSubscribeForm;
