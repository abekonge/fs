/** @jsx jsx */

import { css, jsx } from '@emotion/react';

const Page = ({ children, className }) => (
  <section
    css={css`
      margin: 2rem auto 0 auto;
      width: 700px;
      font-size: 1.18rem;
      line-height: 1.44;
      @media (max-width: 720px) {
        width: auto;
        margin: 2rem 5% 0 5%;
      }
    `}
    className={className}
  >
    {children}
  </section>
);

export default Page;
