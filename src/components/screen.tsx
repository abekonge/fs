/** @jsx jsx */
import { css, jsx } from '@emotion/react';
import { FC, ReactElement } from 'react';

const Screen: FC<{
  header?: ReactElement;
  footer?: ReactElement;
  layout?: [string, string];
  className?: string;
}> = ({
  header,
  footer,
  layout = ['flex-start', 'flex-start'],
  children,
  className,
}) => (
  <section
    className={className}
    css={css`
      display: grid;
      grid-template-rows: auto 1fr auto;
      min-height: 100vh;
    `}
  >
    <header>{header}</header>
    <main
      css={css`
        display: flex;
        flex-direction: column;
        justify-content: ${layout[0]};
        align-items: ${layout[1]};
      `}
    >
      {children}
    </main>
    <footer>{footer}</footer>
  </section>
);

export default Screen;
