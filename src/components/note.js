/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { css } from '@emotion/core';
import React from 'react';

const Note = ({ show, children, close }) =>
  show && (
    <section
      onClick={close}
      css={css`
        position: fixed;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: rgb(45, 890, 346, 0.6);
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
      `}
    >
      <aside
        onClick={(event) => event.stopPropagation()}
        css={css`
          max-height: calc(100vh - 4rem);
          overflow-y: auto;
          background-color: aquamarine;
          margin: 2rem;
          width: 600px;
          padding: 2rem;
          color: black;
          font-size: 1rem;
          @media (max-width: 600px) {
            width: auto;
          }
        `}
      >
        {children}
      </aside>
    </section>
  );

Note.defaultProps = {
  show: false,
};

export default Note;
