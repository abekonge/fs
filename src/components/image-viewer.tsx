/* eslint-disable jsx-a11y/alt-text */
import { css } from '@emotion/core';
import React, { FC, useState } from 'react';
import Bladr from './bladr';

const ImageViewer: FC<{ images: string[] }> = ({ images }) => {
  const [index, setIndex] = useState(0);
  const erDerMere = images.length - 1 > index;
  const erDerMindre = index > 0;
  const bladr = (retning) => {
    if (retning === 'frem') {
      window.scrollTo(0, 0);
      setIndex(erDerMere ? index + 1 : index);
    } else if (retning === 'tilbage') {
      setIndex(erDerMindre ? index - 1 : index);
    }
  };

  return (
    <div
      css={css`
        position: relative;
        display: flex;
        align-items: flex-start;
      `}
    >
      <Bladr
        direction="tilbage"
        action={() => bladr('tilbage')}
        display={erDerMindre}
      />
      <img
        css={css`
          margin: 0;
          width: 100%;
          height: auto;
        `}
        src={images[index]}
      />
      <Bladr
        direction="frem"
        action={() => bladr('frem')}
        display={erDerMere}
      />
    </div>
  );
};

export default ImageViewer;
