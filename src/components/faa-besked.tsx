/* eslint-disable react/jsx-boolean-value */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { css } from '@emotion/core';
import React, { FC, useState } from 'react';
import EmailSubscribeForm from './email-subscribe';
import Modal from './modal';

export const actionStyle = css`
  cursor: pointer;
  :hover {
    text-decoration: underline;
  }
`;

const FaaBesked: FC<{ className: string }> = ({ className = '' }) => {
  const [beskedModal, setBeskedModal] = useState(false);

  return (
    <div className={className}>
      <h3
        onClick={() => setBeskedModal(true)}
        css={css`
          ${actionStyle}
        `}
      >
        få besked
      </h3>
      <Modal open={beskedModal} toggle={() => setBeskedModal(!beskedModal)}>
        <EmailSubscribeForm autoFocus={true} />
      </Modal>
    </div>
  );
};

export default FaaBesked;
