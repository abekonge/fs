/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { css } from '@emotion/core';
import React, { FC } from 'react';

const Modal: FC<{ open: boolean; toggle: () => void }> = ({
  open,
  toggle,
  children,
}) =>
  open && (
    <div
      title="cover"
      onClick={() => toggle()}
      css={css`
        position: fixed;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: rgb(0, 0, 0, 0.6);
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
      `}
    >
      <div onClick={(event) => event.stopPropagation()}>{children}</div>
    </div>
  );

export default Modal;
