import React, { FC } from 'react';
import { Helmet } from 'react-helmet';
import './layout.css';

const Layout: FC = ({ children }) => (
  <main>
    <Helmet>
      <meta charSet="utf-8" />
      <title>FUTURE SCHOOL</title>
      <link rel="canonical" href="http://futureschool.earth" />
      <meta name="description" content="Hvad skal vi lære" />
      <html lang="da" />
      {/* <meta
        property="og:image"
        content="http://futurologi.org/campc/campc.jpg"
      /> */}
      {/* <link
        rel="stylesheet"
        media="screen"
        href="https://fontlibrary.org/face/cmu-sans-serif"
        type="text/css"
      /> */}
    </Helmet>
    {children}
  </main>
);

export default Layout;
