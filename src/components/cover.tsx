import { css } from '@emotion/core';
import React, { FC } from 'react';

const Modal: FC = ({ children }) => (
  <div
    title="cover"
    css={css`
      position: absolute;
      height: 100vh;
      width: 100vw;
      /* background-color: ; */
    `}
  >
    {children}
  </div>
);

export default Modal;
