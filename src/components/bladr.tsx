/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { css } from '@emotion/core';
import React, { FC } from 'react';

const Bladr: FC<{
  direction: 'frem' | 'tilbage';
  action: () => void;
  display: boolean;
}> = ({ direction, action, display }) => (
  <aside
    onClick={action}
    css={css`
      position: absolute;
      ${direction === 'frem' ? 'right:0;' : 'left:0;'}
      z-index: ${display ? 'inherit' : -1};
      height: 100%;
      width: 2rem;
      background-color: black;
      opacity: 0.3;
      cursor: pointer;
      &:hover {
        opacity: 0.6;
        background-color: orchid;
      }
    `}
  >
    <span
      css={css`
        position: fixed;
        padding: 0.1rem;
        user-select: none;
        top: 47%;
        color: white;
        font-size: 2rem;
      `}
    >
      {direction === 'frem' ? '⥹' : '⥺'}
    </span>
  </aside>
);

export default Bladr;
