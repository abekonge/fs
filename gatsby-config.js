module.exports = {
  // Since `gatsby-plugin-typescript` is automatically included in Gatsby you
  // don't need to define it here (just if you need to change the options)
  pathPrefix: `/`,
  siteMetadata: {
    title: `Future School`,
    description: `Future School er et Svendborg/Møn-baseret læringsprojekt, der spørger til, hvad vi har brug for at kunne i fremtiden. Hvad må vi lære for at kunne bebo fremtidens sårbare, ødelagte landskaber med vores sårbare, ødelagte kroppe på nye, gamle, nænsomme, genopbyggende måder? `,
    author: `Future School Collective`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    // {
    //   resolve: `gatsby-source-filesystem`,
    //   options: {
    //     name: `images`,
    //     path: `${__dirname}/src/images`,
    //   },
    // },
    // `gatsby-transformer-sharp`,
    // `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-emotion`,
      options: {
        // Accepts all options defined by `babel-plugin-emotion` plugin.
      },
    },
    // {
    //   resolve: `gatsby-plugin-manifest`,
    //   options: {
    //     name: `Future School`,
    //     short_name: `Future School`,
    //     start_url: `/`,
    //     background_color: `#006d2f`,
    //     theme_color: `#006d2f`,
    //     // Enables "Add to Homescreen" prompt and disables browser UI (including back button)
    //     // see https://developers.google.com/web/fundamentals/web-app-manifest/#display
    //     display: `standalone`,
    //     icon: `src/images/icon.png`, // This path is relative to the root of the site.
    //   },
    // },
    // 'gatsby-plugin-offline',
    // 'gatsby-mdx',
    // {
    //   resolve: `gatsby-source-filesystem`,
    //   options: {
    //     name: `markdown-pages`,
    //     path: `${__dirname}/src/markdown-pages`,
    //   },
    // },
    // `gatsby-transformer-remark`,
  ],
};
